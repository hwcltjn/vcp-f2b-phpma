#!/bin/bash

# Script that adds fail2ban protection for PHPMyadmin on Vesta CP.

# Made by hwcltjn - http://hwclondon.com
# https://gitlab.com/hwcltjn/vcp-f2b-phpma
# Last Update: 09/10/2017

#---- Variables ----#
vestaHostname=$(hostname -f)
config_fail2ban="/etc/fail2ban/jail.local"

# If you need to manually override running the script as root. But why?
root_run="true"
# If you need to manually override running on Ubuntu.
os_override="false"

#---- Root user check ----#
if [ "$root_run" = "true" ] && [ "$EUID" -ne 0 ]; then 
	echo "ERROR: Please run script as root."
	exit 1
fi

#---- Counter -----#
# I'm just genuinely curious as to how many times my script is run :)
# You, your IP, your hardware, your location or any other analytics are NOT being logged/tracked - only how many times the link is hit.
# If you are also curious as to how many times this script has been run, please visit: http://hwcl.net/counter/
curl -s -m 5 https://hwcl.net/counter/count_f2b_phpma.php >/dev/null

#---- Check for Ubuntu ----#
os_check=$(head -n1 /etc/issue | cut -f 1 -d ' ')
if [ "$os_check" != "Ubuntu" ] && [ "$os_override" = "false" ]; then
	echo "ERROR: It doesn't look like your are running Ubuntu. Exiting."
	exit 1
fi

command -v apache2 >/dev/null 2>&1 || { echo "ERROR: Can't find apache2. Exiting." && exit 1 ; }

if [ ! -f /usr/local/vesta/bin/v-add-firewall-chain ]; then
	echo "Error: Vesta Firewall function seems to be missing?. Exiting."
	exit 1
fi

# Backup authcookie.php
mv /usr/share/phpmyadmin/libraries/plugins/auth/AuthenticationCookie.class.php /usr/share/phpmyadmin/libraries/plugins/auth/AuthenticationCookie.class.php_backup

if [ -f /usr/share/phpmyadmin/libraries/plugins/auth/AuthenticationCookie.class.php_backup ]; then
	wget https://gitlab.com/hwcltjn/vcp-f2b-phpma/raw/master/AuthenticationCookie.class.php -O /usr/share/phpmyadmin/libraries/plugins/auth/AuthenticationCookie.class.php
	wget https://gitlab.com/hwcltjn/vcp-f2b-phpma/raw/master/vcp-apache-phpmyadmin.conf -O /etc/fail2ban/filter.d/vcp-apache-phpmyadmin.conf
	wget https://gitlab.com/hwcltjn/vcp-f2b-phpma/raw/master/vcp-phpma.conf -O /etc/fail2ban/action.d/vcp-phpma.conf

	if [ -f $config_fail2ban ]; then
		echo "" >> $config_fail2ban
		echo "[phpmyadmin]" >> $config_fail2ban
		echo "enabled = true" >> $config_fail2ban
		echo "filter = vcp-apache-phpmyadmin" >> $config_fail2ban
		echo "action = vcp-phpma" >> $config_fail2ban
		echo "logpath = /var/log/apache2/domains/$vestaHostname.error.log" >> $config_fail2ban
		echo "findtime = 7200" >> $config_fail2ban
		echo "maxretry = 5" >> $config_fail2ban
		echo "bantime = 900" >> $config_fail2ban

		service fail2ban restart
	    if [ "$?" = "0" ]; then
	    	sleep 2
	    	echo "Fail2ban restarted successfully."
	    	echo "PHPMyAdmin should now be protected!"
	    else
	    	echo "Error: Could not restart fail2ban - something went wrong."
	    fi

	else
		echo "Error: Could not find $config_fail2ban"
	fi
else
	echo "Error: Could not find backup of AuthenticationCookie.class.php. Exiting."
fi

exit 0