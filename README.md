# Vesta CP Fail2Ban jail for PHPMyAdmin

This script adds fail2ban protection for PHPMyAdmin in VestaCP.

#### What this script does:
1 - Adds the code below on line `683` to PHPMyAdmin's Cookie Authentication plugin typically located in `libraries/plugins/auth/AuthenticationCookie.class.php` :
```php
error_log('phpmyadmin: authentification failed from IP: '.$_SERVER['HTTP_X_FORWARDED_FOR'].' ');
```
The above gets PHPMyAdmin to output failed login attempts to Apache's error log.

A backup of `AuthenticationCookie.class.php` is made before modification.

2 - Downloads and configures a straightforward fail2ban [filter](https://gitlab.com/hwcltjn/vcp-f2b-phpma/blob/master/vcp-apache-phpmyadmin.conf) and [action](https://gitlab.com/hwcltjn/vcp-f2b-phpma/blob/master/vcp-phpma.conf) to monitor the default VestaCP hostname Apache error log.

#### Requirements
This modification requires - and has only been tested on:
- Vesta CP 0.9.8-17 running **Apache2 + NGINX**
- Ubuntu 16.0.4
- PHPMyAdmin 4.5.4.1

A version that works on nginx+php-fpm and other distributions should follow.

# Usage
**Use as is - I take no responsability if this damages your server.**

Run the command below as root to download and run the script:
```
wget https://gitlab.com/hwcltjn/vcp-f2b-phpma/raw/master/vcp-f2b-phpma.sh && bash vcp-f2b-phpma.sh

```


#### Credits
- https://blog.louwii.fr/2017/01/secure-phpmyadmin-install-with-fail2ban/